### vi Dockerfile
FROM openjdk:8u302-jdk-oraclelinux8

# copy jar file on to container
COPY target/spring-boot-journal-0.0.1-SNAPSHOT.jar /home/app.jar

WORKDIR /home

CMD  [ "java", "-jar", "app.jar" ]


